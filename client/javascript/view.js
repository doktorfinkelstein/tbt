var gamejs = require('gamejs');
var $v = require('gamejs/utils/vectors');
var objects = require('gamejs/utils/objects');

var UNITS = require('./units').UNITS;

var COLORS = {
   w: '#0000ff',
   p: '#00ff00',
   f: '#005500',
   h: '#cccccc',
   m: '#000000',
   
   c: '#ff0000',
};

var TILE_SIZE = 40;
var TILE_MARGIN = 2;
var TILE_BOX_SIZE = TILE_SIZE + TILE_MARGIN;

var tileToRect = exports.tileToRect = function(pos) {
   return new gamejs.Rect(
      $v.multiply(pos, TILE_BOX_SIZE),
      [TILE_SIZE, TILE_SIZE]
   );
}

var Map = exports.Map = function(data) {

   this.data = data;

   this.draw = function(display) {
      display.blit(this.image);
   };
   
   this.getPossibleTiles = function(unit, functionName) {
      var tiles = [];
      this.data.forEach(function(row, i) {
         row.forEach(function(groundType, j) {
            if (groundType === 'm') return;
            if (UNITS[unit.type.toLowerCase()][functionName].apply(unit, [j, i])) {
               tiles.push([j, i]);
            }
         }, this);
      }, this);
      return tiles;   
   };
   
   this.getPossibleMoveTiles = function(unit) {
      return this.getPossibleTiles(unit, 'canMove');
   };
   
   this.getPossibleAttackTiles = function(unit) {
      return this.getPossibleTiles(unit, 'canAttack');   
   };
   
   
   /**
    * constructor
    */
   var height = this.data.length * TILE_BOX_SIZE;
   var width = this.data[0].length * TILE_BOX_SIZE;
   this.image = new gamejs.Surface(width, height);

   this.data.forEach(function(row, i){
      row.forEach(function(groundType, j) {
         gamejs.draw.rect(this.image, COLORS[groundType], tileToRect([j, i]));
      }, this);
   }, this);
   
   return this;
};


var Unit = exports.Unit = function(config) {
   Unit.superConstructor.apply(this);
   this.forwardX = config.fowardX;
   this.type = config.type;
   this.image = Unit.font.render(this.type);
   var pos = config.pos;
   
   objects.accessors(this, {
      pos: {
         get: function() {
            return pos;
         },
         set: function(newPos) {
            pos = newPos
            this.rect = new gamejs.Rect($v.multiply(pos, TILE_BOX_SIZE), this.image.getSize());
         }
      }
   });
   this.pos = pos;
   
   return this;
};
gamejs.utils.objects.extend(Unit, gamejs.sprite.Sprite);

Unit.font = new gamejs.font.Font('20px courier');
