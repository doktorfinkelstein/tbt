var $v = require('gamejs/utils/vectors');

exports.UNITS = {

   //var footman = {
   f: {
      canMove: function(x, y) {
         return $v.distance([x,y], this.pos) == 1;
      },
      canAttack: function(x, y) {
         return $v.distance([x,y], this.pos) == 1;
      }
   },
   //var knight = {
   k: {
      canMove: function(x, y) {
         return $v.distance([x,y], this.pos) === 2;
      },
      canAttack: function(x, y) {
         return $v.distance([x,y], this.pos) === 1;
      }
   },
   //var archer = {
   a: {
      canMove: function(x, y) {
         return $v.distance([x,y], this.pos) === 1;
      },
      canAttack: function(x, y) {
         var dist = $v.distance([x,y], this.pos);
         // FIXME ugh
         return dist === 2 || dist === 1.4142135623730951;
      },
   },
   // var assassin = {
   s: {
      canMove: function(x, y) {
         return $v.distance([x,y], this.pos) <= 2;
      },
      canAttack: function(x, y) {
         return (x === this.pos[0] - this.forwardX)
               && (y === this.pos[1]);
      },
   },
   //var catapult = {
   c: {
      canMove: function(x, y) {
         return this.pos[0] + this.forwardX === x &&
            (this.pos[1] - y <= 1);
      },
      canAttack: function(x, y) {
         // FIXME 
         // range 2-3 in cardinal directions (except backwards)
         // and range 1-2 forward-diagonally
         return false;
      }
   }
};
