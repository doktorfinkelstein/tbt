var gamejs = require('gamejs');
var view = require('./view');

var events = require('./events');

exports.Network = function(playerId, units) {

   var byTile = function(tile) {
      var unit = null;
      units.some(function(u) {
         if (u.pos[0] === tile[0] && u.pos[1] === tile[1]) {
            unit = u;
            return true;
         }
      });
      return unit;
   };

   this.handle = function(event) {
      // FIXME make async, easy
      if ([events.MOVE_REQUEST, events.ATTACK_REQUEST].indexOf(event.type) > -1) {
         event.playerId = playerId;
         var result = gamejs.http.save('issue', event);
         var unit = byTile(result.payload.unit);
         unit.pos = result.payload.destination;
      }
   }

   return this;
};

/**
 *
 */
exports.Mouse = function(map, units) {

   var selectedUnit = null;
   var possibleMoves = null;
   var possibleAttacks = null;
   
   this.handle = function(event) {
      if (event.type === gamejs.event.MOUSE_UP) {
         if (selectedUnit) {
            // did user issue move order?
            if (possibleMoves) {
               possibleMoves.forEach(function(tile) {
                  if (view.tileToRect(tile).collidePoint(event.pos)) {
                     gamejs.event.post({
                        type: events.MOVE_REQUEST,
                        unit: selectedUnit.pos,
                        destination: tile
                     });
                     possibleMoves = null;
                  }
               });
            } else if (possibleAttacks) {
               // FIXME attack check
            }
         };
         
         // did user click on unit?
         var hits = units.collidePoint(event.pos);
         if (hits.length) {
            // click twice on unit for attack
            if (selectedUnit === hits[0] && !possibleAttacks) {
               selectedUnit = hits[0];
               gamejs.log('unit selected for attack', selectedUnit);
               possibleAttacks = map.getPossibleAttackTiles(selectedUnit);
               // FIXME remove tiles not occupied by enemy
               possibleMoves = null;
            } else {
               selectedUnit = hits[0];
               gamejs.log('unit selected ', selectedUnit);
               possibleMoves = map.getPossibleMoveTiles(selectedUnit);
               // FIXME remove occupied tiles
               possibleAttacks = null;
            }
         }
      }
   };
   
   this.draw = function(display) {
      if (possibleMoves) {
         possibleMoves.forEach(function(tile) {
            gamejs.draw.rect(display, 'rgba(248, 255, 69, 0.8)', view.tileToRect(tile), 0);
         });
      }
      if (possibleAttacks) {
         possibleAttacks.forEach(function(tile) {
            gamejs.draw.rect(display, 'rgba(255, 24, 209, 0.8)', view.tileToRect(tile), 0);
         });
      }
   };

   return this;
};
