var gamejs = require('gamejs');
var $v = require('gamejs/utils/vectors');

var view = require('./view');
var controllers = require('controllers');


gamejs.ready(function() {

   var display = gamejs.display.setMode([600, 400]);
   
   // FIXME make async and let networkcontroller deal with it
   var gameState = gamejs.http.load('join');
   
   var map = new view.Map(gameState.map);
   var units = new gamejs.sprite.Group();
   gameState.units.forEach(function(unitData) {
      units.add(new view.Unit(unitData));
   });
   var mouseController = new controllers.Mouse(map, units);
   var networkController = new controllers.Network(gameState.playerId, units);
   
   function tick(msDuration) {
      gamejs.event.get().forEach(function(event) {
         networkController.handle(event);
         mouseController.handle(event);
      });
      display.clear();
      map.draw(display);
      units.draw(display);
      mouseController.draw(display);
   };

   gamejs.time.fpsCallback(tick, this, 26);
});
