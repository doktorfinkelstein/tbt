// Set up application
var {Application} = require("stick");

var app = exports.app = Application();
app.configure("session", "notfound", "error", "static", "params", "mount");
app.static(module.resolve("client"));
app.mount("/", require("./actions"));

// Script to run app from command line
if (require.main === module) {
   require("ringo/httpserver").main(module.directory);
}
