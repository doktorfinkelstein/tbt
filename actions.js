var {Application} = require("stick");
var response = require("ringo/jsgi/response");
var log = require("ringo/logging").getLogger(module.id);

var events = require('./client/javascript/events');

export("app");

var app = Application();
app.configure("params", "route");


var mapData = [
   'mmmmmmmm',
   'wwwwwwww',
   'pppppppp',
   'hhhhhhhh',
   'pppppppp',
   'pppppppp',
   'ffffffff',
   'hhhhhhhh'
];

var unitData = [
   '........',
   'a.......',
   'k......K',
   '.......C',
   'f......F',
   'f......F',
   'c.......',
   's......S',
   '........',
];

global.game = {
};


// reset units & map
app.get('/reset', function(request) {
   var units = [];
   unitData.forEach(function(row, i) {
      row.split('').forEach(function(unit, j) {
         if (unit === '.') return;
         units.push({
            pos: [j, i],
            type: unit.toLowerCase(),
            playerIdx: unit === unit.toLowerCase() ? 0 : 1
         })
      });
   });
   global.game = {
      units: units,
      map: mapData.map(function(row) {
         return row.split('');
      }),
      players: [],
   };
   log.info('Game reset');
   return response.json({
      'status': 'success!'
   });
});

app.get('/join', function(request) {
   var playerId = Math.random() * Date.now();
   request.session.data.playerId = playerId;
   global.game.players.push(playerId);
   log.info('Player joined', playerId);
   return response.json({
      playerId: playerId,
      units: global.game.units,
      map: global.game.map
   });
});

app.post('/issue', function(request) {
   // FIXME security by comparing
   // request.session.data.playerId with event data
   // FIXME validate that the movements is valid
   var resData = request.postParams;
   resData.type = events.MOVE_RESPONSE;
   return response.json(resData);
});
